use std::io;
use std::io::*;
use std::fmt::Write;

#[derive(Debug, Clone)]
struct Node {
    size: u16,
    symbol: char,
    children: Vec<Node>
}

fn count_symbols(input: String) -> Vec<Node> {
    let mut vector: Vec<Node> = vec![];

    for i in 0..input.len() {
        let mut foundSymbol: bool = false;
            for n in 0..vector.len() {
                if Some(vector[n].symbol) == input.chars().nth(i as usize) {
                    vector[n].size += 1;
                    foundSymbol = true;
                    break;
                }
            }
     if !foundSymbol {vector.push(Node {symbol: input.chars().nth(i as usize).unwrap(), size:1, children:vec![] })}

    }

    return vector;
}

fn sort_vector(mut vector: Vec<Node>) -> Vec<Node> {
    for i in 1..vector.len() {
        let current_item = vector[i].clone();
        let mut j: isize = i as isize - 1;
        while j >= 0 && vector[j as usize].size < current_item.size {
            vector[j as usize + 1] = vector[j as usize].clone();

            j = j - 1;
        }
        (vector[(j + 1) as usize]) = current_item;
    }

    return vector;
}

fn merge_vector(mut vector: Vec<Node>) -> Node {
    while vector.len() > 1 {
        // First: sorting vector
        vector = sort_vector(vector);


        // Second: get minimal items
        let last_item = vector.pop().unwrap();
        let before_last_item = vector.pop().unwrap();


        // Third: merge items
        vector.push(Node {size:last_item.size + before_last_item.size, children:vec![last_item.clone(), before_last_item.clone()], symbol:' ' })


    }
    return vector[0].clone();
}

fn retrieve_codes(tree: Node) -> Vec<(char, Vec<u8>)> {
let mut result:Vec<(char, Vec<u8>)> = vec![];

for i in 0..tree.children.len() {
    let item = tree.children[i].clone();
    if item.children.len() == 0 {
        result.push((item.symbol, vec![i as u8]));
    }
    else{
        let mut children = retrieve_codes(item);
        for j in 0..children.len() {
            children[j].1.push(i as u8);
            result.push(children[j].clone());
        }
    }
}

return result;
}

fn encode(input: String, alphabet: Vec<(char, String)>) -> String {
    let mut output: String = String::new();


    for i in 0..input.len() {
        let current_symbol:char = input.chars().nth(i).unwrap();
        for j in 0..alphabet.len() {
            if alphabet[j].0 == current_symbol {
                write!(output, "{}", alphabet[j].1);
            }
        }
    }
    return output;
}


fn format_codes(codes:Vec<(char, Vec<u8>)>) -> Vec<(char, String)> {
    let mut formatted_codes = vec![];

    for i in 0..codes.len() {
        let item = codes[i].clone();
        let ch = item.0;
        let reversed_code = item.1;
        let mut string = String::new();

        for j in 0..reversed_code.len() {
            write!(string, "{}", reversed_code[reversed_code.len() - j - 1]);
        }
        let item = (ch, string);
        formatted_codes.push(item);
    }
    

    return formatted_codes;
}

fn main() {
    let stdin = io::stdin();
    let mut iterator = stdin.lock().lines();
    loop {
    println!("Enter string (english-only): ");
    let input = iterator.next().unwrap().unwrap();

    let tree: Node = merge_vector(count_symbols(input.clone()));

    let codes = retrieve_codes(tree);

    let result = format_codes(codes);

    for (ch, code) in result.iter() {
        println!("{}: {}", ch, code);
    } 
    println!("{}", encode(input, result));
}
}
